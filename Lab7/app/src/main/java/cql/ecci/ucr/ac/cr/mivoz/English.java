package cql.ecci.ucr.ac.cr.mivoz;

import java.util.Locale;

/**
 * Clase para representar el idioma inglés según el esquema de Language.
 * */
class English extends Language {
    /**
     * Constructor que llama al constructor padre con el lenguaje inglés.
     * */
    English(){
        super("en-US", Locale.US, AvailableLanguages.EN);
    }
}
