package cql.ecci.ucr.ac.cr.mivoz;

import java.util.Locale;

/**
 * Clase para representar el idioma español según el esquema de Language.
 * */
class Spanish extends Language {
    /**
     * Constructor que llama al constructor padre con el lenguaje español.
     * */
    Spanish(){
        super("es-ES", new Locale("spa", "ESP"), AvailableLanguages.ES);
    }
}
