package cql.ecci.ucr.ac.cr.mivoz;

import java.util.Locale;

/**
 * Clase base para representar un idioma, ya sea inglés o español.
 * Contiene el nombre del idioma y el objeto Locale correspondiente.
 * */
class Language {
    private String language;
    private Locale locale;
    private AvailableLanguages currentLanguage;
    protected enum AvailableLanguages {
        EN,
        ES
    }

    /**
     * Constructor.
     * @param language Representación String del lenguaje.
     * @param locale Objeto Locale del lenguaje indicado en el parámetro anterior.
     * @param lang Identificador del lenguaje.
     * */
    Language(String language, Locale locale, AvailableLanguages lang) {
        this.language = language;
        this.locale = locale;
        this.currentLanguage = lang;
    }

    /**
     * Obtiene la representación String del lenguaje.
     * @return String con el nombre del lenguaje.
     * */
    String getLanguage() {
        return language;
    }

    /**
     * Obtiene el objeto Locale correspondiente al lenguaje.
     * @return Locale correspondiente al lenguaje actual.
     * */
    Locale getLocale() {
        return locale;
    }

    /**
     * Construye y retorna un objeto Language con el lenguaje contrario al indicado por parámetro.
     * @param language lenguaje que se quiere invertir.
     * @return objeto Language con el lenguaje contrario al indicado por parámetro.
     * */
    static Language switchLanguage(Language language){
        Language newLanguage;
        if(language.currentLanguage == AvailableLanguages.ES){
            newLanguage = new Language("en-US", Locale.US, AvailableLanguages.EN);
        }else{
            newLanguage = new Language("es-ES", new Locale("spa", "ESP"), AvailableLanguages.ES);
        }
        return newLanguage;
    }

    /**
     * Método para saber si este lenguaje es inglés o español.
     * @return true si el lenguaje es inglés, false si es español.
     * */
    boolean isEnglish(){
        return this.currentLanguage == AvailableLanguages.EN;
    }
}
