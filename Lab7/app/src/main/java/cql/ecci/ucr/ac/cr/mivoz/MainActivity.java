package cql.ecci.ucr.ac.cr.mivoz;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions;
import com.google.firebase.ml.naturallanguage.FirebaseNaturalLanguage;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslateLanguage;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslator;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslatorOptions;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // Tipos de fuente de texto a traducir.
    private enum Source {
        SPEECH,
        TEXT
    }
    // Codigo de retorno del intent
    private static final int REQUEST_CODE = 1;
    // Lenguaje
    private Language language;
    // Texto y botones iniciales de la actividad principal
    TextView mTexto;
    // Opcion de Voz a texto
    Button mSpeechText;
    // Lista de textos encontrados
    ArrayList<String> mMatchesText;
    // Lista para mostrar los textos
    ListView mTextListView;
    // Dialogo para mostrar la lista
    Dialog mMatchTextDialog;
    // Instancia de Texto a voz
    TextToSpeech mTextToSpeech;
    // Texto de entrada
    EditText mEditText;
    // Opcion de Texto a voz
    Button mTextSpeech;
    // Checkbox para habilitar o deshabilitar la traducción
    CheckBox mTranslateEnabled;
    // Traductor usando servicios de Firebase
    FirebaseTranslator translator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // El lenguaje por defecto es inglés
        this.language = new English();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // componentes de la aplicacion
        mSpeechText = findViewById(R.id.buttonSpeechText);
        mTextSpeech = findViewById(R.id.buttonTextSpeech);
        mTexto = findViewById(R.id.texto);
        mEditText = findViewById(R.id.editText);
        mTranslateEnabled = findViewById(R.id.translate);
        // intent para el reconocimiento de voz
        mSpeechText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isConnected()) {
                    // intent al API de reconocimiento de voz
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language.getLanguage());
                    startActivityForResult(intent, REQUEST_CODE);
                } else {
                    Toast.makeText(getApplicationContext(), "No hay conexión a Internet",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Set el lenguaje de texto a voz
        mTextToSpeech = new TextToSpeech(getApplicationContext(), new
                TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        mTextToSpeech.setLanguage(language.getLocale());
                    }
                });
        // Intente para texto a voz
        mTextSpeech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // lenguaje
                mTextToSpeech.setLanguage(language.getLocale());
                // Texto el edit
                String toSpeak = mEditText.getText().toString();
                // Mayor que Lollipop
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    //Si el traductor está habilitado, se dice la tira de texto traducida al idioma contrario
                    if(mTranslateEnabled.isChecked()){
                        translate(toSpeak, Source.TEXT);
                    }else {
                        mTextToSpeech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, null);
                        Toast.makeText(getApplicationContext(), toSpeak, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        if(mTranslateEnabled.isChecked())
            initTranslator(language);
    }

    /**
     * Habilita o deshabilita los botones de texto a voz y voz a texto.
     * @param enable true si se deben habilitar los botones, false en caso contrario.
     * */
    private void enableButtons(boolean enable){
        mSpeechText = findViewById(R.id.buttonSpeechText);
        mTextSpeech = findViewById(R.id.buttonTextSpeech);
        if(mSpeechText != null && mTextSpeech != null){
            mSpeechText.setEnabled(enable);
            mTextSpeech.setEnabled(enable);
        }
    }

    /**
     * Muestra o esconde la barra de progreso y el mensaje de "Cargando".
     * @param show true si se deben mostrar, false en caso contrario.
     * */
    private void showStatus(boolean show) {
        ProgressBar progressBar = findViewById(R.id.progressBar);
        TextView textView = findViewById(R.id.statusText);
        if(progressBar != null && textView != null){
            if(show){
                progressBar.setVisibility(View.VISIBLE);
                textView.setVisibility(View.VISIBLE);
            }else {
                progressBar.setVisibility(View.INVISIBLE);
                textView.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * Inicializa el traductor de Firebase con el lenguaje dado por parámetro
     * El lenguaje origen de traducción es el pasado por parámetro, y el de destino es el contrario.
     * @param lang Objeto Language con el que se va a inicializar el traductor.
     * */
    private void initTranslator(Language lang) {
        showStatus(true);
        FirebaseTranslatorOptions options;
        if(lang.isEnglish())
            options = new FirebaseTranslatorOptions.Builder()
                    .setSourceLanguage(FirebaseTranslateLanguage.EN)
                    .setTargetLanguage(FirebaseTranslateLanguage.ES)
                    .build();
        else
            options = new FirebaseTranslatorOptions.Builder()
                    .setSourceLanguage(FirebaseTranslateLanguage.ES)
                    .setTargetLanguage(FirebaseTranslateLanguage.EN)
                    .build();

        translator = FirebaseNaturalLanguage.getInstance().getTranslator(options);

        FirebaseModelDownloadConditions conditions = new FirebaseModelDownloadConditions.Builder()
                .requireWifi()
                .build();
        translator.downloadModelIfNeeded(conditions)
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void v) {
                                /*Se habilitan los botones y se esconde la barra de progreso después del callback.*/
                                enableButtons(true);
                                showStatus(false);
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                /*Se habilitan los botones y se esconde la barra de progreso después del callback.*/
                                enableButtons(true);
                                Toast.makeText(getApplicationContext(), "No se pudo inicializar el traductor.",
                                        Toast.LENGTH_SHORT).show();
                                showStatus(false);
                            }
                        });
    }

    /**
     * Evento que se dispara cuando se marca o desmarca una casilla tipo radio.
     * @param view Vista en la que se encuentra el radio button.
     * */
    public void onRadioButtonClicked(View view) {
        // Verificar el RadioButton seleccionado
        boolean checked = ((RadioButton) view).isChecked();
        // Determinar cual RadioButton esta seleccionado
        switch (view.getId()) {
            case R.id.radioButtonIngles:
                if (checked){
                    language = new English();
                    if(mTranslateEnabled.isChecked()) {
                        /*Si el traductor está habilitado, se inicializa el traductor de inglés a español.*/
                        enableButtons(false);
                        initTranslator(language);
                    }
                }
                break;
            case R.id.radioButtonEspannol:
                if (checked) {
                    language = new Spanish();
                    if(mTranslateEnabled.isChecked()) {
                        /*Si el traductor está habilitado, se inicializa el traductor de español  a inglés.*/
                        enableButtons(false);
                        initTranslator(language);
                    }
                }
                break;
        }
    }

    /**
     * Inicializa el traductor cuando se marca la casilla del mismo.
     * @param view Vista en la que se encuentra el checkbox.
     * */
    public void onCheckButtonClicked(View view){
        if(mTranslateEnabled.isChecked()){
            enableButtons(false);
            initTranslator(language);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Valores de retorn del intent
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            // Si retorna resultados el servicion de reconocimiento de voz
            // Creamos el dialogo y asignamos la lista
            mMatchTextDialog = new Dialog(MainActivity.this);
            mMatchTextDialog.setContentView(R.layout.dialog_matches_frag);
            // título del dialogo
            mMatchTextDialog.setTitle("Seleccione el texto");
            // Lista de elementos
            mTextListView = mMatchTextDialog.findViewById(R.id.listView1);
            mMatchesText = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if(mMatchesText != null) {
                // Mostramos los datos en la lista
                ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                        android.R.layout.simple_list_item_1, mMatchesText);
                mTextListView.setAdapter(adapter);
                mMatchTextDialog.show();
            }
            // Asignamos el evento del clic en la lista
            mTextListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long
                        id) {
                    /*Si la traducción está activada, se escribe la tira seleccionada traducida al idioma correspondiente.*/
                    if(mTranslateEnabled.isChecked()){
                        translate(mMatchesText.get(position), Source.SPEECH);
                    }else {
                        mTexto.setText(String.format(getResources().getString(R.string.text), mMatchesText.get(position)));
                        mEditText.setText(mMatchesText.get(position));
                        mMatchTextDialog.hide();
                    }
                }
            });

        }
    }

    /**
     * Verifica la conexión a Internet.
     * @return true si hay conexión, false en caso contrario.
     * */
    public boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService
                (Context.CONNECTIVITY_SERVICE);
        if(cm != null) {
            NetworkInfo net = cm.getActiveNetworkInfo();
            return net != null && net.isAvailable() && net.isConnected();
        }
        return false;
    }

    /**
     * Traduce una tira de texto al idioma contrario de la variable global language, y lo manda a la
     * salida correspondiente según lo indique el parámetro source.
     * @param text Texto a traducir al idioma contrario a la variable global language.
     * @param source Fuente de la tira de texto.
     * */
    private void translate(String text, final Source source){
        translator.translate(text)
                .addOnSuccessListener(
                        new OnSuccessListener<String>() {
                            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                            @Override
                            public void onSuccess(@NonNull String translatedText) {
                                switch(source){
                                    case SPEECH:
                                        /*Si viene del diálogo de selección del speech, se escribe en el editText y debajo del appbar.*/
                                        mTexto.setText(String.format(getResources().getString(R.string.text), translatedText));
                                        mEditText.setText(translatedText);
                                        mMatchTextDialog.hide();
                                        break;

                                    case TEXT:
                                        /*Si viene del EditText, se manda la traducción al "speaker"*/
                                        mTextToSpeech.setLanguage(Language.switchLanguage(language).getLocale());
                                        mTextToSpeech.speak(translatedText, TextToSpeech.QUEUE_FLUSH, null, null);
                                        Toast.makeText(getApplicationContext(), translatedText, Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getApplicationContext(), "No se pudo traducir.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });


    }
}